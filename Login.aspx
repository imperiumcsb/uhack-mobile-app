﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Med Master</title>
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <link href="../Content/font-awesome.min.css" rel="stylesheet" />
</head>
<body>
    <div class="col-lg-12">

        <div class="col-lg-offset-3 col-lg-6">

            <form id="RegisterForm" runat="server">

           
                
                <div class="form-group">
                    <div class="col-lg-4">
                        <label class="control-label">User Name:</label>
                    </div>
                    <div class="col-lg-8">
                        <asp:TextBox ID="txtName" MaxLength="180" CssClass="form-control" runat="server"  required></asp:TextBox>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-4">
                        <label class="control-label">Password:</label>
                    </div>
                    <div class="col-lg-8">
                        <asp:TextBox ID="txtPW" MaxLength="64" CssClass="form-control" TextMode="Password" runat="server" required></asp:TextBox>
                    </div>
                </div>

            

                <div class="form-group">
                    <div class="col-lg-12">
                        <asp:Button ID="btnRegister" CssClass="btn-success pull-right btn-lg" Text="Login" runat="server" Onclick="btnRegister_Click" OnClientClick="return alert('Signed In Sucessfully!');" />
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>
<script src="../Scripts/jquery-3.2.1.min.js"></script>
<script src="../Scripts/bootstrap.min.js"></script>
</html>
