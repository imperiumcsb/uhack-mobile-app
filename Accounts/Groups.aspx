﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Groups.aspx.cs" Inherits="Accounts_Groups" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table>
            <tr>
                <th>Group</th>
                <th>Payment Interval</th>
                <th>Payment Due</th>
                <th>Amount</th>
                <th>Queue</th>
                <th></th>
            </tr>
<asp:ListView ID="lvGroups" runat="server">
    <ItemTemplate>
        <tr>
            <td><%# Eval("GroupDetailID") %></td>
            <td><%# Eval("PaymentTerm") %></td>
            <td>Every <%# Eval("CashOut") %> Days</td>
            <td><%# Eval("PaymentAmount") %></td>
            <td><%# Eval("NumberOnQueue") %></td>
<td><a href='transfer.aspx?id=<%# Eval("GroupDetailID") %>'>Contribute</a></td>
        </tr>

    </ItemTemplate>
    <EmptyDataTemplate>
        <tr>
            <td colspan="5"><h1>No Groups Joined yet.</h1></td>
        </tr>

    </EmptyDataTemplate>
</asp:ListView>    
    </div>
    </form>
</body>
</html>
