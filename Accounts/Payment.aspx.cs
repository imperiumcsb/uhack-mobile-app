﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;

public partial class Accounts_Payment : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnTerms_Click(object sender, EventArgs e)
    {
        using (SqlConnection conn = new SqlConnection(Helper.GetConnection()))
        {
            conn.Open();
            string sql = @"SELECT TOP 1 * FROM GroupDetails WHERE PaymentTerm = @interval AND PaymentAmount = @type AND CashOut = @cashout AND NumberOfMembers < @number";
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@interval", ddlInterval.SelectedValue.ToString());
                cmd.Parameters.AddWithValue("@type", ddlType.SelectedValue.ToString());
                cmd.Parameters.AddWithValue("@cashout", ddlCashout.SelectedValue.ToString());
                cmd.Parameters.AddWithValue("@number", 12);
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if(dr.HasRows)
                    {
while(dr.Read())
                        {
                            using (SqlConnection conn2 = new SqlConnection(Helper.GetConnection()))
                            {
                                conn2.Open();
                                string sql2 = @"SELECT MAX(NumberOnQueue) AS Queue FROM UserDetails WHERE GroupDetailID = @group";
                                using (SqlCommand cmd2 = new SqlCommand(sql2, conn2))
                                {
                                    cmd2.Parameters.AddWithValue("@group", dr["ID"].ToString());
                                    System.Diagnostics.Debug.WriteLine("Group: " + dr["ID"].ToString());
                                    using (SqlDataReader dr2 = cmd2.ExecuteReader())
                                    {
                                        if(dr2.HasRows)
                                        {
                                            while(dr2.Read())
                                            {
                                                int queue_number = Convert.ToInt32(dr2["Queue"]);
                                                queue_number += 1;
                                                System.Diagnostics.Debug.WriteLine("Number: " + queue_number);
                                                using (SqlConnection conn3 = new SqlConnection(Helper.GetConnection()))
                                                {
                                                    conn3.Open();
                                                    string sql3 = @"INSERT INTO UserDetails (GroupDetailID, AccountID, NumberOnQueue) VALUES (@group, @user, @queue)";
                                                    using (SqlCommand cmd3 = new SqlCommand(sql3, conn3))
                                                    {
                                                        //cmd3.Parameters.AddWithValue("@newID", SqlDbType.Int).Direction = ParameterDirection.Output;
                                                        cmd3.Parameters.AddWithValue("@group", dr["ID"].ToString());
                                                        cmd3.Parameters.AddWithValue("@user", Session["ID"].ToString());
                                                        cmd3.Parameters.AddWithValue("@queue", queue_number);
                                                        cmd3.ExecuteNonQuery();
                                                        System.Diagnostics.Debug.WriteLine("Queue: " + queue_number + " Spark: " + dr["ID"].ToString());
                                                        using (SqlConnection conn4 = new SqlConnection(Helper.GetConnection()))
                                                        {
                                                            conn4.Open();
                                                            string sql4 = @"UPDATE GroupDetails SET NumberOfMembers = NumberOfMembers+1 WHERE ID = @group";
                                                            using (SqlCommand cmd4 = new SqlCommand(sql4, conn4))
                                                            {
                                                                cmd4.Parameters.AddWithValue("@group", dr["ID"].ToString());
                                                                cmd4.ExecuteNonQuery();
                                                                Response.Redirect("Default.aspx");
                                                            } //closing command 4.
                                                        } //closing conn4.
                                                    } //closing command 3.
                                                } //closing connection 3.
} //closing while 2.
                                        } //closing hasRows.
                                    } //closing dr2.
                                } //closing command 2.
                            } //closing connection 2.
                            
                        } //closing while.
                    } //closing if.
                    else
                    {
                        using(SqlConnection  conn5 = new SqlConnection(Helper.GetConnection()))
                            {
                            conn5.Open();
                            string sql5 = @"INSERT INTO GroupDetails (PaymentTerm, PaymentAmount, CashOut, NumberOfMembers, Status, DateCommenced) VALUES (@interval, @type, @cashout, @members, @status, @date)" + "SET @newId = SCOPE_IDENTITY()";
                            using (SqlCommand cmd5 = new SqlCommand(sql5, conn5))
                            {
                                cmd5.Parameters.AddWithValue("@interval", ddlInterval.SelectedValue);
                                cmd5.Parameters.AddWithValue("@type", ddlType.SelectedValue);
                                cmd5.Parameters.AddWithValue("@cashout", ddlCashout.SelectedValue);
                                cmd5.Parameters.AddWithValue("@members", 1);
                                cmd5.Parameters.AddWithValue("@status", 1);
                                cmd5.Parameters.AddWithValue("@date", "1960-01-11");
                                cmd5.Parameters.Add("@newId", SqlDbType.Int).Direction = ParameterDirection.Output;
                                cmd5.ExecuteScalar();
                                int scope_id = (int)cmd5.Parameters["@newId"].Value;
                                using (SqlConnection conn6 = new SqlConnection(Helper.GetConnection()))
                                {
                                    conn6.Open();
                                    string sql6 = "INSERT INTO UserDetails (GroupDetailID, AccountID, NumberOnQueue) VALUES (@group, @user, @queue) SET @newID = Scope_Identity()";
                                    using (SqlCommand cmd6 = new SqlCommand(sql6, conn6))
                                    {
                                        cmd6.Parameters.AddWithValue("@newID", SqlDbType.Int).Direction = ParameterDirection.Output;

                                        cmd6.Parameters.AddWithValue("@group", scope_id);
                                        cmd6.Parameters.AddWithValue("@user", Session["ID"].ToString());
                                        cmd6.Parameters.AddWithValue("@queue", 1);
                                        
                                        cmd6.ExecuteNonQuery();
                                    }
                                }

                                    } //closing command 5.
                                } //closing conn5.

                    } //closing else.
} //closing reader.
            } //closing command.
        } //closing connection.
    }
}
