﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Payment.aspx.cs" Inherits="Accounts_Payment" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Med Master</title>
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <link href="../Content/font-awesome.min.css" rel="stylesheet" />
</head>
<body>
    <div class="col-lg-12">

        <div class="col-lg-offset-3 col-lg-6">

            <form id="RegisterForm" runat="server">

                       <div class="form-group">
                    <div class="col-lg-4">
                        <label class="control-label">Payment interval:</label>
                    </div>
                    <div class="col-lg-8">
                        <asp:DropDownList CssClass="form-control" ID="ddlInterval" runat="server">
                            <asp:ListItem Value="">Select...</asp:ListItem>
                            <asp:ListItem Value="Daily">Daily</asp:ListItem>
                            <asp:ListItem Value="Weekly">Weekly</asp:ListItem>
</asp:DropDownList>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-4">
                        <label class="control-label">Amount to pay:</label>
                    </div>
                    <div class="col-lg-8">
                        <asp:DropDownList CssClass="form-control" ID="ddlType" runat="server">
                            <asp:ListItem Value="">Select...</asp:ListItem>
                            <asp:ListItem Value="20">20</asp:ListItem>
                            <asp:ListItem Value="50">50</asp:ListItem>
                             <asp:ListItem Value="100">100</asp:ListItem>
                            <asp:ListItem Value="200">200</asp:ListItem>
                                   <asp:ListItem Value="500">500</asp:ListItem>
             </asp:DropDownList>
                    </div>
                </div>
                
                
                       <div class="form-group">
                    <div class="col-lg-4">
                        <label class="control-label">Cash out:</label>
                    </div>
                    <div class="col-lg-8">
                        <asp:DropDownList CssClass="form-control" ID="ddlCashout" runat="server">
                            <asp:ListItem Value="">Select...</asp:ListItem>
                            <asp:ListItem Value="15">After 15 Days</asp:ListItem>
                            <asp:ListItem Value="30">After 30 Days</asp:ListItem>
                 </asp:DropDownList>
                    </div>
                </div>
                
         
                
                



                <div class="form-group">
                    <div class="col-lg-12">
                        <asp:Button ID="btnTerms" CssClass="btn-success pull-right btn-lg" Text="Submit" runat="server" OnClick="btnTerms_Click" OnClientClick="return alert('Signed In Successfully!');" />
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>
<script src="../Scripts/jquery-3.2.1.min.js"></script>
<script src="../Scripts/bootstrap.min.js"></script>
</html>