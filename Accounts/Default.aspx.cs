﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using System.Data.SqlClient;


public partial class Accounts_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        using (SqlConnection conn = new SqlConnection(Helper.GetConnection()))
        {
            conn.Open();
            string sql = @"SELECT * FROM Accounts WHERE ID = @user_id";
            using (SqlCommand cmd = new SqlCommand(sql, conn))
            {
                cmd.Parameters.AddWithValue("@user_id", Session["ID"].ToString());
                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    if(dr.HasRows)
                    {
                        while(dr.Read())
                        {
                            ltAccountName.Text = dr["FullName"].ToString();
                            ltAccountNumber.Text = dr["AccountNumber"].ToString();
                            ltAccountType.Text = dr["AccountType"].ToString();
                            ltAvailableBalance.Text = dr["Balance"].ToString();
} //closing read.
                    }

                } //closing reader.
            } //closing cmd.
        } //closing connection.
    }

    protected void btnRegister_Click(object sender, EventArgs e)
    {

    }
}