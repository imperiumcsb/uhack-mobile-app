﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Accounts_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Med Master</title>
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <link href="../Content/font-awesome.min.css" rel="stylesheet" />
</head>
<body>
    <div class="col-lg-12">

        <div class="col-lg-offset-3 col-lg-6">

            <form id="RegisterForm" runat="server">
                <label>Account Name: </label>
                <asp:Literal ID="ltAccountName" runat="server"></asp:Literal>
                <br />
                <label>Account Number: </label>
                <asp:Literal ID="ltAccountNumber" runat="server"></asp:Literal>
                <br />
                <label>Account Type: </label>
                <asp:Literal ID="ltAccountType" runat="server"></asp:Literal>
                <br />
                <label>Available Balance: </label>
                <asp:Literal ID="ltAvailableBalance" runat="server"></asp:Literal>
                
                
            </form>
        </div>
    </div>
</body>
<script src="../Scripts/jquery-3.2.1.min.js"></script>
<script src="../Scripts/bootstrap.min.js"></script>
</html>