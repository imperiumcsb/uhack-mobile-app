﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Register.aspx.cs" Inherits="Register" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Med Master</title>
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
    <link href="../Content/font-awesome.min.css" rel="stylesheet" />
</head>
<body>
    <div class="col-lg-12">

        <div class="col-lg-offset-3 col-lg-6">

            <form id="RegisterForm" runat="server">

                <div class="form-group">
                    <div class="col-lg-4">
                        <label class="control-label">Account Number:</label>
                    </div>
                    <div class="col-lg-8">
                        <asp:TextBox ID="txtAccNo" MaxLength="60" CssClass="form-control" runat="server"  required></asp:TextBox>
                    </div>
                </div>

                
                <div class="form-group">
                    <div class="col-lg-4">
                        <label class="control-label">Full Name:</label>
                    </div>
                    <div class="col-lg-8">
                        <asp:TextBox ID="txtName" MaxLength="180" CssClass="form-control" runat="server"  required></asp:TextBox>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-4">
                        <label class="control-label">Password:</label>
                    </div>
                    <div class="col-lg-8">
                        <asp:TextBox ID="txtPW" MaxLength="64" CssClass="form-control" TextMode="Password" runat="server" required></asp:TextBox>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-4">
                        <label class="control-label">Confirm Password:</label>
                    </div>
                    <div class="col-lg-8">
                        <asp:TextBox ID="txtCPW" MaxLength="64" CssClass="form-control" TextMode="Password" runat="server" required />
                        <asp:CompareValidator ID="cvPW" runat="server"
                            ControlToValidate="txtCPW" ControlToCompare="txtPW"
                            Display="Dynamic" ErrorMessage="Not Match"
                            ForeColor="Red" SetFocusOnError="true" />
                    </div>
                </div>

               
                <div class="form-group">
                    <div class="col-lg-4">
                        <label class="control-label">User Name:</label>
                    </div>
                    <div class="col-lg-8">
                        <asp:TextBox ID="txtUser" MaxLength="60" CssClass="form-control" runat="server"  required></asp:TextBox>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-4">
                        <label class="control-label">Account Code:</label>
                    </div>
                    <div class="col-lg-8">
                        <asp:TextBox ID="txtCode" MaxLength="35" CssClass="form-control" runat="server"></asp:TextBox>
                    </div>
                </div>

             

                <div class="form-group">
                    <div class="col-lg-4">
                        <label class="control-label">Type:</label>
                    </div>
                    <div class="col-lg-8">
                        <asp:DropDownList CssClass="form-control" ID="type" runat="server">
                            <asp:ListItem Value="Savings">Savings</asp:ListItem>
                                    <asp:ListItem Value="Current">Current</asp:ListItem>

                        </asp:DropDownList>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-12">
                        <asp:Button ID="btnRegister" CssClass="btn-success pull-right btn-lg" Text="Register" runat="server" OnClick="btnRegister_Click" OnClientClick="return alert('Registered Sucessfully!');" />
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>
<script src="../Scripts/jquery-3.2.1.min.js"></script>
<script src="../Scripts/bootstrap.min.js"></script>
</html>
