﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Register : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnRegister_Click(object sender, EventArgs e)
    {
        using (SqlConnection conn = new SqlConnection(Helper.GetConnection()))
        {
            conn.Open();
            string SQL = @"INSERT INTO Accounts(AccountNumber,FullName,AccountType,AccountCode,Balance,Status,username,password)
VALUES (@Accno, @FullName, @Type,  @Code, @Balance,@Status, @username,@password)";

            using (SqlCommand cmd = new SqlCommand(SQL, conn))
            {

                cmd.Parameters.AddWithValue("@Accno", txtAccNo.Text);
                cmd.Parameters.AddWithValue("@FullName", txtName.Text);
                cmd.Parameters.AddWithValue("@Type", type.SelectedValue);
                cmd.Parameters.AddWithValue("@Code", txtCode.Text);
                cmd.Parameters.AddWithValue("@Balance", 5000);
                cmd.Parameters.AddWithValue("@Status", "Active");
                cmd.Parameters.AddWithValue("@username", txtUser.Text);
                cmd.Parameters.AddWithValue("@password", Helper.CreateSHAHash(txtPW.Text));

                cmd.ExecuteNonQuery();





            }
            conn.Close();
        }

        Response.Redirect("Login.aspx");
    }
}